import firebase from 'firebase/app';

import 'firebase/firestore';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyAgz0rpLU8Ynm3nazSIaSchiutMOmb2SAk",
    authDomain: "crwn-clothing-953d6.firebaseapp.com",
    databaseURL: "https://crwn-clothing-953d6.firebaseio.com",
    projectId: "crwn-clothing-953d6",
    storageBucket: "crwn-clothing-953d6.appspot.com",
    messagingSenderId: "869881001088",
    appId: "1:869881001088:web:c68fa44626654b767be265",
    measurementId: "G-794YBHJ68C"
};

export const createUserProfileDocument = async (userAuth, additionalData) => {
    if (!userAuth) return;

    const userRef = firestore.doc(`users.${userAuth.uid}`);

    const snapshot = await userRef.get();

    if (!snapshot.exists) {
        const { displayName, email } = userAuth;
        const createdAt = new Date();

        try {
            await userRef.set({
                displayName,
                email,
                createdAt,
                ...additionalData
            })
        } catch (error) {
            console.log('error creating user', error.message);
        }

    }

    return userRef;
}


firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();

provider.setCustomParameters({ prompt: 'select_account' });

export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;

